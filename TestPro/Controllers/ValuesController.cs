﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace TestPro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("AllowAllOrigins")]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<DataTransferObject> Get()
        {
            return Ok(new DataTransferObject());
        }
    }
}
