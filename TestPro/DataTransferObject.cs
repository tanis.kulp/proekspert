﻿using TestPro.Services;

namespace TestPro
{
    public class DataTransferObject
    {

        public DataTransferObject()
        {
            DataReceived = DataItem.DataReceived;
            Source = DataItem.Source;
            Active = DataItem.Active.Translate();
            Phone = DataItem.Phone;
            XlService = DataItem.XlService.Translate(nameof(this.XlService));
            Language = DataItem.Language.Translate(nameof(this.Language));
            XlLanguage = DataItem.XlLanguage.Translate(nameof(this.Language));
            End = DataItem.End?.ToString() ?? "undefined";
            XlActivation = DataItem.XlActivation?.ToString() ?? "undefined";
            XlEnd = DataItem.XlEnd?.ToString() ?? "undefined";
            OverrideList = DataItem.OverrideList.Translate(nameof(this.OverrideList));
            PhoneNumbers = DataItem.PhoneNumbers;
            Names = DataItem.Names;
        }

        public bool DataReceived { get; set; }

        public string Source { get; set; }

        public string Active { get; set; }

        public string Phone { get; set; }

        public string XlService { get; set; }

        public string Language { get; set; }

        public string XlLanguage { get; set; }

        public string End { get; set; }

        public string XlActivation { get; set; }

        public string XlEnd { get; set; }

        public string OverrideList { get; set; }

        public string[] PhoneNumbers { get; set; }

        public string[] Names { get; set; }
    }
}
