﻿namespace TestPro.Services
{
    public static class SymbolsTranslator
    {
        public static string Translate(this string symbol, string property = "")
        {
            switch (symbol)
            {
                case "A": return "Active";
                case "P": return "Passive";
                case "J": return "Yes";
                case "I": return "English";
                case "K": return "In use";
                case "E":
                    switch (property)
                    {
                        case "XlService": return "No";
                        case "Language": return "Estonian";
                        case "OverrideList": return "Not in use";
                        default: return symbol;
                    }
                default: return symbol;
            }
        }
    }
}
