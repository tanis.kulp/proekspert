﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestPro.Services
{
    interface IDataReader
    {
        string GetData(int counter);

        void ParseData(string text);
    }
}
