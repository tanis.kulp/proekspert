﻿using NodaTime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace TestPro.Services
{
    public class DataReader : IDataReader
    {
        private static int counter = 1;
        internal Timer Timer;

        public DataReader() { Timer = new Timer(RunCommands, null, 0, 5000); }

        private void RunCommands(object timerState)
        {
            var text = GetData(counter);

            if (text != null)
                ParseData(text);

            if (counter != 9)
            {
                counter++;
            }
            else
                counter = 1;
        }

        public string GetData(int counter)
        {
            var client = new WebClient();
            var address = "http://people.proekspert.ee/ak/data_" + counter + ".txt";
            Stream stream;
            try
            {
                stream = client.OpenRead(address);
            }
            catch
            {
                DataItem.DataReceived = false;
                DataItem.Source = address;
                return null;
            }
            var reader = new StreamReader(stream);

            DataItem.DataReceived = true;

            DataItem.Source = address;

            return reader.ReadToEnd();
        }

        public void ParseData(string text)
        {
            var active = text.Length >= 1 ? text.Substring(0, 1) : "UD";
            if (active.ToUpper() == "A" || active.ToUpper() == "P")
                DataItem.Active = active.ToUpper();
            else
            {
                DataItem.Active = "undefined";
            }

            DataItem.Phone = text.Length >= 21 ? text.Substring(1, 20).Trim() : "UD";
            if (!int.TryParse(DataItem.Phone, out _))
                DataItem.Phone = "undefined";

            var xl = text.Length >= 22 ? text.Substring(21, 1) : "UD";
            if (xl.ToUpper() == "J" || xl.ToUpper() == "E")
                DataItem.XlService = xl.ToUpper();
            else
            {
                DataItem.XlService = "undefined";
            }

            var supportedLanguages = new List<string>() { "E", "I" };
            var language = text.Length >= 23 ? text.Substring(22, 1) : "UD";
            DataItem.Language = supportedLanguages.Contains(language.ToUpper()) ? language.ToUpper() : "undefined";

            var xlLanguage = text.Length >= 24 ? text.Substring(23, 1).ToUpper() : "UD";
            DataItem.XlLanguage = supportedLanguages.Contains(xlLanguage.ToUpper()) ? xlLanguage.ToUpper() : "undefined";

            DataItem.End = null;
            var endYear = text.Length >= 29 ? text.Substring(24, 4) : "UD";
            var endMonth = text.Length >= 30 ? text.Substring(28, 2) : "UD";
            var endDay = text.Length >= 32 ? text.Substring(30, 2) : "UD";
            if (int.TryParse(endYear, out var year) && int.TryParse(endMonth, out var month) && int.TryParse(endDay, out var day))
            {
                var endHour = text.Length >= 34 ? text.Substring(32, 2) : "UD";
                if (!int.TryParse(endHour, out var hour))
                    hour = 0;
                var endMinute = text.Length >= 36 ? text.Substring(34, 2) : "UD";
                if (!int.TryParse(endMinute, out var minute))
                    minute = 0;

                DataItem.End = new DateTime(year, month, day, hour, minute, 0);
            }

            DataItem.XlActivation = null;
            var xlActivationHour = text.Length >= 38 ? text.Substring(36, 2) : "UD";
            var xlActivationMinute = text.Length >= 40 ? text.Substring(38, 2) : "UD";

            if (int.TryParse(xlActivationHour, out var xlActHour) && int.TryParse(xlActivationMinute, out var xlActMinute))
            {
                DataItem.XlActivation = new LocalTime(xlActHour, xlActMinute);
            }

            DataItem.XlEnd = null;
            var xlEndHour = text.Length >= 42 ? text.Substring(40, 2) : "UD";
            var xlEndMinute = text.Length >= 44 ? text.Substring(42, 2) : "UD";

            if (int.TryParse(xlEndHour, out var xlEndHourResult) && int.TryParse(xlEndMinute, out var xlEndMinuteResult))
            {
                DataItem.XlEnd = new LocalTime(xlEndHourResult, xlEndMinuteResult);
            }

            var overrideList = text.Length >= 45 ? text.Substring(44, 1) : "UD";
            if (active.ToUpper() == "K" || active.ToUpper() == "E")
                DataItem.OverrideList = active.ToUpper();
            else
            {
                DataItem.OverrideList = "undefined";
            }

            DataItem.PhoneNumbers = new string[8];
            for (var i = 45; i <= 165; i += 15)
            {
                var j = (i - 45) / 15;
                var phoneNumber = text.Length >= i + 15 ? text.Substring(i, 15).Trim() : "UD";

                if (int.TryParse(phoneNumber, out _))
                    DataItem.PhoneNumbers[j] = phoneNumber;
            }

            DataItem.Names = new string[8];
            for (var i = 165; i <= 325; i += 20)
            {
                var j = (i - 165) / 20;
                var name = string.Empty;

                if (text.Length >= i + 20)
                {
                    name = text.Substring(i, 20).Trim();
                }
                if (name != string.Empty)
                    DataItem.Names[j] = name;
            }

            if (counter == 9)
            {
                var outPut = new DataTransferObject();
            }
        }

    }
}
