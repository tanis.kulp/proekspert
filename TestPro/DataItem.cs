﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NodaTime;

namespace TestPro
{
    public static class DataItem
    {
        public static bool DataReceived { get; set; }

        public static string Source { get; set; }

        public static string Active { get; set; }

        public static string Phone { get; set; }

        public static string XlService { get; set; }

        public static string Language { get; set; }

        public static string XlLanguage { get; set; }

        public static DateTime? End { get; set; }

        public static LocalTime? XlActivation { get; set; }

        public static LocalTime? XlEnd { get; set; }

        public static string OverrideList { get; set; }

        public static string[] PhoneNumbers { get; set; }

        public static string[] Names { get; set; }
    }
}
